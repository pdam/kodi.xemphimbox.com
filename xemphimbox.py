import urllib, urllib2, re, os, sys
import xbmcaddon,xbmcplugin,xbmcgui
import requests, json
import string
from bs4 import BeautifulSoup
import SimpleDownloader as downloader
import xbmcvfs


REMOTE_DBG = False

# append pydev remote debugger
if REMOTE_DBG:
    # Make pydev debugger works for auto reload.
    # Note pydevd module need to be copied in XBMC\system\python\Lib\pysrc
    try:
        import sys
        sys.path.append('D:\Program Files (x86)\Kodi\system\python\Lib\pysrc')
        import pydevd
    # stdoutToServer and stderrToServer redirect stdout and stderr to eclipse console
        pydevd.settrace('localhost', stdoutToServer=True, stderrToServer=True)
    except ImportError:
        sys.stderr.write("Error: " +
            "You must add org.python.pydev.debug.pysrc to your PYTHONPATH.")
        sys.exit(1)

homelink = 'http://xemphimbox.com/'
addon = xbmcaddon.Addon()
addonID = addon.getAddonInfo('id')
addonname = addon.getAddonInfo('name')
mysettings = xbmcaddon.Addon(id='plugin.video.xemphimbox')
icon = addon.getAddonInfo('icon')
mysettings = xbmcaddon.Addon(id='plugin.video.xemphimbox')
downloader = downloader.SimpleDownloader()
downloadpath = mysettings.getSetting('download_path')
home = mysettings.getAddonInfo('path')
logo = addon.getAddonInfo('icon')
#xemphimbox variables
AjaxURL = 'http://xemphimbox.com/ajax'
GrabURL = 'http://grab.xemphimbox.com/player.js.php'

addonUserDataFolder = xbmc.translatePath("special://profile/addon_data/"+addonID).decode('utf-8')
libraryFolder = os.path.join(addonUserDataFolder, "library")
libraryFolderMovies = os.path.join(libraryFolder, "Movies")
libraryFolderTV = os.path.join(libraryFolder, "TV")
custLibFolder = mysettings.getSetting('library_path')
custLibTvFolder = mysettings.getSetting('libraryTV_path')

searchlink = 'http://xemphimbox.com/tim-kiem/'

if not os.path.exists(os.path.join(addonUserDataFolder, "settings.xml")):
    addon.openSettings()
if not os.path.isdir(addonUserDataFolder):
    os.mkdir(addonUserDataFolder)
if not os.path.isdir(libraryFolder):
    os.mkdir(libraryFolder)
if not os.path.isdir(libraryFolderMovies):
    os.mkdir(libraryFolderMovies)
if not os.path.isdir(libraryFolderTV):
    os.mkdir(libraryFolderTV)

def GetYouTubeVideoID(url):
    """Returns Video_ID extracting from the given url of Youtube
    
    Examples of URLs:
      Valid:
        'http://youtu.be/_lOT2p_FCvA',
        'www.youtube.com/watch?v=_lOT2p_FCvA&feature=feedu',
        'http://www.youtube.com/embed/_lOT2p_FCvA',
        'http://www.youtube.com/v/_lOT2p_FCvA?version=3&amp;hl=en_US',
        'https://www.youtube.com/watch?v=rTHlyTphWP0&index=6&list=PLjeDyYvG6-40qawYNR4juzvSOg-ezZ2a6',
        'youtube.com/watch?v=_lOT2p_FCvA',
      
      Invalid:
        'youtu.be/watch?v=_lOT2p_FCvA',
    """

    from urlparse import urlparse, parse_qs

    if url.startswith(('youtu', 'www')):
        url = 'http://' + url
        
    query = urlparse(url)
    
    if 'youtube' in query.hostname:
        if query.path == '/watch':
            return parse_qs(query.query)['v'][0]
        elif query.path.startswith(('/embed/', '/v/')):
            return query.path.split('/')[2]
    elif 'youtu.be' in query.hostname:
        return query.path[1:]
    else:
        raise ValueError
	
def GetContent(url):
    headers = {'User-agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16'}
    response = requests.get(url,headers=headers)
    return response.text

def GetVideoLinks(code):
	code = code.replace('&', '%26')
	#headers = {'User-agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16', 'Content-type':'application/x-www-form-urlencoded', 'Referer': 'http://m.phimvipvn.net/xem-phim/co-hoi-thu-2-phan-1-second-chance-season-1.70982/', 'Cookie': '__cfduid=de12404029d113b4b1a2fced781bb189c1453520542; PHPSESSID=25v3t8hdffkbo1bv6lngrvrgh4; _ga=GA1.2.509004442.1453520552', 'Origin': 'http://m.phimvipvn.net', 'Host': 'm.phimvipvn.net'}
	headers = {'User-agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16', 'Content-type':'application/x-www-form-urlencoded'}
	links = requests.post(GrabURL, data={'file':code}, headers=headers)
	return links.text
	
def GetVideoScriptURL(source): 	
	episodeIDregex = r'filmInfo.episodeID = parseInt\(\'(.*?)\'\);'
	episodeID = re.compile(episodeIDregex).findall(source)[0]
	filmIDregx = r'filmInfo.filmID = parseInt\(\'(.*?)\'\);'
	filmID = re.compile(filmIDregx).findall(source)[0]	
	headers = {'User-agent':'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16', 'Content-type':'application/x-www-form-urlencoded'}
	response = requests.post(AjaxURL, data={'NextEpisode': 1, 'EpisodeID': episodeID, 'filmID': filmID, 'playTech': 'auto'}, headers=headers)
	soup = BeautifulSoup(response.text)
	script = soup('script')[1]
	return script['src']

def homeDirs(url, ulclass):	
	link = GetContent(url)
	soup = BeautifulSoup(link)		
	slists = soup('ul',{'class':ulclass})
	for s in slists:
		sli = BeautifulSoup(str(s))('li')
		for l in sli:
			lsoup = BeautifulSoup(str(l))
			llink = lsoup('a')[0]['href']
			lname = lsoup('a')[0].contents[0]
			addDir(lname.encode('utf-8'), llink, 1, logo, False, None,'')
			
def home():
	addDir('[COLOR ffffd700]Search[/COLOR]',searchlink,5,logo,False,None,'')
	homeDirs(homelink, 'sub')

def category(url):
    link = GetContent(url)
    soup = BeautifulSoup(link)
    content_items = soup('div',{'class':'item col-lg-3 col-md-3 col-sm-6 col-xs-6'})
    for citem in content_items:
        cisoup = BeautifulSoup(str(citem))
        cname = cisoup('a')[0]['title'] + " - " + str(cisoup('dfn')[1].contents[0])
        clink = cisoup('a')[0]['href']
        cimage = cisoup('img')[0]['src']
        # cinfo = cisoup('li')[3].next
        gname = cname
        addDir(cname.encode('utf-8'), clink.encode('utf-8'), 2, cimage, False, None,gname.encode('utf-8'))
    try:
        pagination = soup('span',{'class':'page_nav'})
        page = BeautifulSoup(str(pagination[0]))('span')
        for p in page:
            psoup = BeautifulSoup(str(p))
            try:
                pactive=psoup('span',{'class':'current'})[0]
            except:
                ptitle = psoup('a')[0].contents[0]
                plink = psoup('a')[0]['href']
                addDir(ptitle.encode('utf-8'), plink.encode('utf-8'), 1, logo, False, None,'')
    except:pass

def serverlist(url):
	link = GetContent(url)
	soup = BeautifulSoup(link)
	serverlink = BeautifulSoup(str(soup('div',{'class':'watch'})))('a')[0]['href']

	newlink = GetContent(serverlink)
	nsoup = BeautifulSoup(newlink)
	server_item = nsoup('div', {'class':'server'})	
	inum = 0	
	for si in range(0, len(server_item)):
		server = server_item[si]('div', {'class':'name'})
		siname = BeautifulSoup(str(server))('div')[0].text
		addDir(siname.encode('utf-8'), serverlink, 3, iconimage,False, inum,gname)
		inum += 1

def episode(url):
    link = GetContent(url)
    soup = BeautifulSoup(link)
    server_item = soup('div', {'class':'server'})[inum]
    span = BeautifulSoup(str(server_item))('a')
    for s in span:
        ssoup = BeautifulSoup(str(s))
        try:
            s['class']
            sname = str(ssoup('a')[0].contents[0]).replace("<b>","").replace("</b>","")
            slink = ssoup('a')[0]['href']
            addLink(sname, slink, 4, iconimage,gname,inum)
        except KeyError:
            slink = ssoup('a')[0]['href']
            sname = str(ssoup('a')[0].contents[0]).replace("<b>","").replace("</b>","")
            addLink(sname.encode('utf-8'), slink, 4, iconimage,gname,inum)

def medialink(url):
	link = GetContent(url)
	soup = BeautifulSoup(link)	
	videoScriptURL = GetVideoScriptURL(link)
	vscriptsrc = GetContent(videoScriptURL)	
	linkcoderegex = r'"file":"(.*?)",'
	vlinks = re.compile(linkcoderegex).findall(vscriptsrc)	
	#get highest resolution
	vlink = vlinks[len(vlinks)-1].replace("\\","")
	xbmc.log('My Debug medialink:' + vlink)
	#vlink = "https://redirector.googlevideo.com/videoplayback?requiressl=yes&id=a613428ca9045bc5&itag=18&source=webdrive&app=texmex&ip=2400:6180:0:d0::565:2001&ipbits=32&expire=1465643941&sparams=requiressl%2Cid%2Citag%2Csource%2Cip%2Cipbits%2Cexpire&signature=5D67EF8345E8DDD8B0ADEFFE27F6EBED3E7A1B48.7B6B6C34FB8DB3204C752A21D2C7B804B537CE44&key=ck2&mm=30&mn=sn-npo7enel&ms=nxu&mt=1465629396&mv=m&nh=IgpwcjAyLnNpbjAzKgkxMjcuMC4wLjE&pl=48"
	return vlink

def play(url,name):
    VideoUrl = medialink(url)
    print VideoUrl
    if VideoUrl.find('youtube')!=-1:        
        VideoUrl = GetYouTubeVideoID(VideoUrl)
        VideoUrl = "plugin://plugin.video.youtube?path=/root/video&action=play_video&videoid="+urllib.quote_plus(VideoUrl).replace('?','')

    listitem = xbmcgui.ListItem(name, path=VideoUrl)
    listitem.setInfo( type="Video", infoLabels={ "Title": name })
    xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, listitem)
    # xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, xbmcgui.ListItem(path=VideoUrl))

def download(url):
    try:
        VideoUrl = medialink(url)

        if VideoUrl.find('youtube')==-1:
            filename = gname+'.mp4'
            print 'Start downloading '+filename
            print VideoUrl
            params = {"url": VideoUrl, "download_path": downloadpath, "Title": gname}
            if os.path.isfile(downloadpath+filename):
                dialog = xbmcgui.Dialog()
                if dialog.yesno('Download message','File exists! re-download?'):
                    downloader.download(filename, params)
            else:
                downloader.download(filename, params)

    except:pass

def addToLibrary(url):
    if mysettings.getSetting('cust_Lib_path')=='true':
        newlibraryFolderMovies = custLibFolder
    else:
        newlibraryFolderMovies = libraryFolderMovies
    movieFolderName = (''.join(c for c in unicode(gname, 'utf-8') if c not in '/\\:?"*|<>')).strip(' .')
    newMovieFolderName=''
    finalName=''
    keyb = xbmc.Keyboard(name, '[COLOR ffffd700]Enter Title[/COLOR]')
    keyb.doModal()
    if (keyb.isConfirmed()):
        newMovieFolderName=keyb.getText()
    if newMovieFolderName !='':
        dir = os.path.join(newlibraryFolderMovies, newMovieFolderName)
        finalName=newMovieFolderName
    else:
        dir = os.path.join(newlibraryFolderMovies, movieFolderName)
        finalName=movieFolderName

    if not os.path.isdir(dir):
        xbmcvfs.mkdir(dir)
        fh = xbmcvfs.File(os.path.join(dir, finalName+".strm"), 'w')
        fh.write('plugin://'+addonID+'/?mode=4&url='+urllib.quote_plus(url)+'&name='+urllib.quote_plus(finalName))
        fh.close()
        # xbmc.executebuiltin('UpdateLibrary(video)')

def addSeasonToLibrary(url):
    if mysettings.getSetting('cust_LibTV_path')=='true':
        newlibraryFolderMovies = custLibTvFolder
    else:
        newlibraryFolderMovies = libraryFolderTV
    movieFolderName = (''.join(c for c in unicode(gname, 'utf-8') if c not in '/\\:?"*|<>')).strip(' .')
    newMovieFolderName=''
    finalName=''
    keyb = xbmc.Keyboard(name, '[COLOR ffffd700]Enter Title[/COLOR]')
    keyb.doModal()
    if (keyb.isConfirmed()):
        newMovieFolderName=keyb.getText()
    if newMovieFolderName !='':
        dir = os.path.join(newlibraryFolderMovies, newMovieFolderName)
        finalName=newMovieFolderName
    else:
        dir = os.path.join(newlibraryFolderMovies, movieFolderName)
        finalName=movieFolderName

    keyb = xbmc.Keyboard(name, '[COLOR ffffd700]Enter Season[/COLOR]')
    keyb.doModal()
    if (keyb.isConfirmed()):
        seasonnum=keyb.getText()

    link = GetContent(url)
    soup = BeautifulSoup(link)
    server_item = soup('div', {'class':'server_item'})[inum]
    span = BeautifulSoup(str(server_item))('span')

    if not os.path.isdir(dir):
        xbmcvfs.mkdir(dir)
        for s in span:
            ssoup = BeautifulSoup(str(s))
            try:
                s['class']
                sname = '1'
                if len(sname)==1:
                    epnum = '0'+sname
                else:
                    epnum=etitle
                epname = ''.join(["S", seasonnum, "E", epnum, ' - ', finalName])
                slink = ssoup('a')[0]['href']
                fh = xbmcvfs.File(os.path.join(dir, epname+".strm"), 'w')
                fh.write('plugin://'+addonID+'/?mode=4&url='+urllib.quote_plus(slink)+'&name='+urllib.quote_plus(''.join(["[", sname.encode('utf-8'), "] ", gname])))
                fh.close()
            except KeyError:
                slink = ssoup('a')[0]['href']
                sname = ssoup('a')[0].contents[0]
                if len(sname)==1:
                    epnum = '0'+sname
                else:
                    epnum=sname
                epname = ''.join(["S", seasonnum, "E", epnum, ' - ', finalName])
                fh = xbmcvfs.File(os.path.join(dir, epname+".strm"), 'w')
                fh.write('plugin://'+addonID+'/?mode=4&url='+urllib.quote_plus(slink)+'&name='+urllib.quote_plus(''.join(["[", sname.encode('utf-8'), "] ", gname])))
                fh.close()
    else:
        dialog = xbmcgui.Dialog()
        if dialog.yesno('TV Show Exists','Update Files?'):
            for s in span:
                ssoup = BeautifulSoup(str(s))
                try:
                    s['class']
                    sname = '1'
                    if len(sname)==1:
                        epnum = '0'+sname
                    else:
                        epnum=etitle
                    epname = ''.join(["S", seasonnum, "E", epnum, ' - ', finalName])
                    slink = ssoup('a')[0]['href']
                    fh = xbmcvfs.File(os.path.join(dir, epname+".strm"), 'w')
                    fh.write('plugin://'+addonID+'/?mode=4&url='+urllib.quote_plus(slink)+'&name='+urllib.quote_plus(''.join(["[", sname.encode('utf-8'), "] ", gname])))
                    fh.close()
                except KeyError:
                    slink = ssoup('a')[0]['href']
                    sname = ssoup('a')[0].contents[0]
                    if len(sname)==1:
                        epnum = '0'+sname
                    else:
                        epnum=sname
                    epname = ''.join(["S", seasonnum, "E", epnum, ' - ', finalName])
                    fh = xbmcvfs.File(os.path.join(dir, epname+".strm"), 'w')
                    fh.write('plugin://'+addonID+'/?mode=4&url='+urllib.quote_plus(slink)+'&name='+urllib.quote_plus(''.join(["[", sname.encode('utf-8'), "] ", gname])))
                    fh.close()


######## SEARCH ###############################################################################################
def loadHistory(url):
    try:
        addDir('[COLOR ffffd700]Search[/COLOR]',url,6,logo,False,None,'')
        if mysettings.getSetting('save_search')=='true':
            searches = getStoredSearch()
            if len(searches)!=0:
                searches = eval(searches)
                idn = 0
                for s in searches:
                    addDir(s,url+urllib.quote_plus(s),1,logo,True,idn,'')
                    idn+=1
    except:pass

def deleteSearch():
    try:
        searches = getStoredSearch()
        searches = eval(searches)
        del(searches[inum])
        saveStoredSearch(searches)
    except StopIteration:
        pass

def editSearch():
    try:
        searches = getStoredSearch()
        searches = eval(searches)
        keyb = xbmc.Keyboard(name, '[COLOR ffffd700]Enter search text[/COLOR]')
        keyb.doModal()
        if (keyb.isConfirmed()):
            newsearch = keyb.getText()
            searches[inum]=newsearch
        saveStoredSearch(searches)
        newsearch=urllib.quote_plus(newsearch)
        xbmc.executebuiltin("XBMC.Container.Refresh")
        Search(searchlink+newsearch)
    except:pass

def getUserInput():
    try:
        searches = getStoredSearch()
        keyb = xbmc.Keyboard('', '[COLOR ffffd700]Enter search text[/COLOR]')
        keyb.doModal()
        if (keyb.isConfirmed()):
            searchText = urllib.quote_plus(keyb.getText())
            url = searchlink+ searchText
            if mysettings.getSetting('save_search')=='true':
                if searchText!='':
                    if len(searches)==0:
                        searches = ''.join(["['",urllib.unquote_plus(searchText),"']"])
                        searches = eval(searches)
                    else:
                        searches = eval(searches)
                        searches = [urllib.unquote_plus(searchText)] + searches
                    saveStoredSearch(searches)
        return url
    except:pass

def Search(url):
    try:
        if url.find('.html')!=-1:
            url = url.rstrip()
        else:
            url = getUserInput()
        xbmc.executebuiltin("XBMC.Container.Refresh")
        category(url)
    except: pass

def getStoredSearch():
    try:
        searches = mysettings.getSetting('store_searches')
        return searches
    except:pass

def saveStoredSearch(param):
    try:
        mysettings.setSetting('store_searches',repr(param))
    except:pass
######## END SEARCH #########################################################################################

def addDir(name, url, mode, iconimage,edit, inum, gname):
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)+"&inum="+str(inum)+"&gname="+urllib.quote_plus(gname)
    ok=True
    liz=xbmcgui.ListItem(name, iconImage=logo, thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name })
    liz.setArt({'thumb': iconimage,'icon': logo,'fanart': iconimage})    
    contextmenuitems = []
    if edit:
        contextmenuitems.append(('[COLOR red]Delete[/COLOR] [COLOR ffffd700]Search[/COLOR]','XBMC.Container.Update(%s?url=%s&mode=8&name=%s&inum=%d)'%('plugin://plugin.video.xemphimbox',urllib.quote_plus(url),urllib.quote_plus(name),inum)))
        contextmenuitems.append(('[COLOR ff00ff00]Edit[/COLOR] [COLOR ffffd700]Search[/COLOR]','XBMC.Container.Update(%s?url=%s&mode=9&name=%s&inum=%d)'%('plugin://plugin.video.xemphimbox',urllib.quote_plus(url),urllib.quote_plus(name),inum)))
        liz.addContextMenuItems(contextmenuitems,replaceItems=False)
    ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)	
    return ok

def addLink(name,url,mode,iconimage,gname,inum):
    name = ''.join(["[", name, "] ", gname])
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)+"&gname="+urllib.quote_plus(gname)+"&inum="+str(inum)
    liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name})
    liz.setArt({'thumb': iconimage,'icon': logo,'fanart': iconimage})
    liz.setProperty('mimetype', 'video/x-msvideo')
    liz.setProperty("IsPlayable","true")
    contextmenuitems = []
    contextmenuitems.append(('[COLOR yellow]Download[/COLOR]','XBMC.Container.Update(%s?url=%s&gname=%s&mode=10)'%('plugin://plugin.video.xemphimbox',urllib.quote_plus(url),urllib.quote_plus(gname))))
    contextmenuitems.append(('[COLOR ff1e90ff]Add To Library[/COLOR]','XBMC.Container.Update(%s?url=%s&gname=%s&mode=11)'%('plugin://plugin.video.xemphimbox',urllib.quote_plus(url),urllib.quote_plus(gname))))
    contextmenuitems.append(('[COLOR ff1e90ff]Add Season To Library[/COLOR]','XBMC.Container.Update(%s?url=%s&gname=%s&inum=%s&mode=12)'%('plugin://plugin.video.xemphimbox',urllib.quote_plus(url),urllib.quote_plus(gname), urllib.quote_plus(str(inum)))))
    liz.addContextMenuItems(contextmenuitems,replaceItems=False)
    ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz, isFolder=False)
    return ok

def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]

        return param

params=get_params()
url=None
name=None
mode=None
iconimage=None
edit=None
inum=None
gname=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        iconimage=urllib.unquote_plus(params["iconimage"])
except:
        pass
try:
        mode=int(params["mode"])
except:
        pass
try:
        edit = bool(params["edit"])
except:
        pass
try:
        inum=int(params["inum"])
except:
        pass
try:
        gname=urllib.unquote_plus(params["gname"])
except:
        pass

sysarg=str(sys.argv[1])

if mode==None or url==None or len(url)<1:
    home()
elif mode==1:    
    category(url)
    xbmc.executebuiltin("Container.SetViewMode(500)")
elif mode==2:    
    serverlist(url)    
elif mode==3:
    episode(url)
    xbmc.executebuiltin("Container.SetViewMode(500)")
elif mode==4:
    play(url,name)
elif mode==5:
    loadHistory(url)
elif mode==6:
    Search(url)
elif mode==7:
    episodes(url)
elif mode==8:
    deleteSearch()
elif mode==9:
    editSearch()
elif mode==10:
    download(url)
elif mode==11:
    addToLibrary(url)
elif mode==12:
    addSeasonToLibrary(url)

xbmcplugin.endOfDirectory(int(sysarg))
